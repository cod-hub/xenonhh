import React from "react";
import style from "./select.module.scss";
import { Image, Form } from "react-bootstrap";
import { CommonInput } from "../../components";
import { ToggleTabs } from "../../components/Toggletab";

function Selection() {
  return (
    <div className={style.Selectionmain}>
      {/* <CommonLayout> */}
      <div className={style.selectdiv}>
        <p className={style.webuserinfo}>
          Select your <span className={style.websiteheading}>website</span>
        </p>

        <div className={style.toggletabs}>
          <ToggleTabs />
        </div>

        <div className={style.settingdiv}>
          <Form>
            <div>
              <label>
                Long Setting 1
                <Form.Check
                  type="switch"
                  id="custom-switch-1"
                  className={style.customswitch}
                />
              </label>
            </div>
            <div>
              <label>
                Long Setting 2
                <Form.Check
                  type="switch"
                  id="custom-switch-2"
                  className={style.customswitch}
                />
              </label>
            </div>
            <div>
              <label>
                Long Setting 3
                <Form.Check
                  type="switch"
                  id="custom-switch-3"
                  className={style.customswitch}
                />
              </label>
            </div>
          </Form>

          <div className={style.webhook}>
            <CommonInput
              label="Discord Webhook"
              placeholder="+123456789"
              type="number"
            />
          </div>
        </div>
      </div>
      {/* </CommonLayout> */}
      <div className={style.hhdiv}>
        <Image src="/images/hh.svg" />
      </div>
    </div>
  );
}
export { Selection };
