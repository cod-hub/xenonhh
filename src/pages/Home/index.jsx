import React, { useState } from "react";
import { Button, Form, Row, Col } from "react-bootstrap";
import style from "./home.module.scss";

function Home() {
  const [cardExpiry, SetCardExpiry] = useState("");
  const [creditCard, SetCreditCard] = useState("");

  const expiryDate = (target) => {
    if (target.value) {
      let value = target.value.split("/").join(""); // Remove dash (:) if mistakenly entered.
      let finalVal = value.match(/.{1,2}/g).join("/");
      target.value = finalVal;
      console.log(finalVal);
      SetCardExpiry(finalVal);
    } else {
      SetCardExpiry(target.value);
    }
  };
  const patternCard = (target) => {
    if (target.value) {
      let value = target.value.split("-").join(""); // Remove dash (:) if mistakenly entered.
      console.log(value, "asdasdasd");
      let finalVal = value.match(/.{1,4}/g).join("-");
      target.value = finalVal;
      console.log(finalVal);
      SetCreditCard(finalVal);
    } else {
      SetCreditCard(target.value);
    }
  };

  const maxLengthCheck = (object) => {
    if (object.target.value.length > object.target.maxLength) {
      object.target.value = object.target.value.slice(
        0,
        object.target.maxLength
      );
    }
  };
  return (
    <div className={style.homemain}>
      <Form className={style.form}>
        <Row>
          <Col md={12}>
            <p className={style.para1}>
              Your personal
              <span className={style.personalheading}> information</span>
            </p>
          </Col>
          <Col md={6}>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>First name</Form.Label>
              <Form.Control type="text" placeholder="Enter First Name" />
            </Form.Group>
          </Col>
          <Col md={6}>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Last Name</Form.Label>
              <Form.Control type="text" placeholder="Enter Last Name" />
            </Form.Group>
          </Col>
          <Col md={6}>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>E-mail</Form.Label>
              <Form.Control type="email" placeholder="Enter Email" />
            </Form.Group>
          </Col>
          <Col md={6}>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Phone #</Form.Label>
              <Form.Control type="number" placeholder="Enter Phone Number" />
            </Form.Group>
          </Col>

          <Col md={12}>
            <p className={style.para2}>
              Where you want your order
              <span className={style.personalheading}> shipped?</span>
            </p>
          </Col>
          <Col md={6}>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Address 1</Form.Label>
              <Form.Control type="text" placeholder="Enter Address 1" />
            </Form.Group>
          </Col>
          <Col md={6}>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Address 2</Form.Label>
              <Form.Control type="text" placeholder="Enter Address 2" />
            </Form.Group>
          </Col>
          <Col md={6}>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>City</Form.Label>
              <Form.Control type="email" placeholder="Enter City" />
            </Form.Group>
          </Col>
          <Col md={6}>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>ZIP</Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter ZIP Code"
                maxLength="6"
                onInput={maxLengthCheck}
              />
            </Form.Group>
          </Col>
          <Col md={6}>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Country</Form.Label>
              <Form.Control type="text" placeholder="Enter Country" />
            </Form.Group>
          </Col>

          <Col md={12}>
            <p className={style.para3}>
              Where you want your order
              <span className={style.personalheading}> shipped?</span>
            </p>
          </Col>
          <Col md={12}>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Card #</Form.Label>
              <Form.Control
                value={creditCard}
                type="text"
                placeholder="Enter Card #"
                maxLength={19}
                onChange={({ target }) => {
                  patternCard(target);
                }}
              />
            </Form.Group>
          </Col>
          <Col md={6}>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Expire Date</Form.Label>
              <Form.Control
                value={cardExpiry}
                type="text"
                maxLength={5}
                placeholder="Enter Expire Date"
                onChange={({ target }) => {
                  expiryDate(target);
                }}
              />
            </Form.Group>
          </Col>
          <Col md={6}>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>CVV</Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter CVV"
                maxLength={3}
                onInput={maxLengthCheck}
              />
            </Form.Group>
          </Col>
        </Row>
        <div className={style.submitbtn}>
          <Button variant="default" type="">
            Cancel
          </Button>
          <Button variant="default" type="submit">
            Submit
          </Button>
        </div>
      </Form>
    </div>
  );
}

export { Home };
