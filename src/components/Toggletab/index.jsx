import React from "react";
import "./toggle.css";

function ToggleTabs() {
  return (
    <div className="toggle-btn">
      {/* <label className="checkout-label">Checkout Mode</label> */}
      <div>
        <nav className="radio-togglestwo">
          <input type="radio" id="option-1" name="radio-options" />
          <label for="option-1">Shopify</label>
          <input type="radio" id="option-2" name="radio-options" checked />
          <label for="option-2">Newera</label>
          <div className="radio-togglestwo__slider"></div>
        </nav>
      </div>
    </div>
  );
}

export { ToggleTabs };
