import React from "react";
import { Tabs, Tab } from "react-bootstrap";
import { Home, Selection } from "../../pages";
import style from "./nav.module.scss";

function TabLink() {
  return (
    <div className={style.nav}>
      <Tabs variant="tabs" defaultActiveKey="home" className={style.navtabs}>
        <Tab eventKey="home" title="Page 1">
          <Home />
        </Tab>
        <Tab eventKey="/selection" title="Page 2">
          <Selection />
        </Tab>
      </Tabs>
    </div>
  );
}

export { TabLink };
