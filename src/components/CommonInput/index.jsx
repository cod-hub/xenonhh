import React from "react";
import style from "./input.scss";
import { Form } from "react-bootstrap";

function CommonInput({ label, placeholder, type }) {
  return (
    <div className={style.inputmain}>
      <Form>
        <Form.Group controlId="formBasicEmail">
          <Form.Label>{label}</Form.Label>
          <Form.Control type={type} placeholder={placeholder} />
        </Form.Group>
      </Form>
    </div>
  );
}

export { CommonInput };
