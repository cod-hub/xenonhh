import React from "react";
import style from "./header.module.scss";
import { Button, Image } from "react-bootstrap";
import { TabLink } from "../../components";

function Header() {
  return (
    <>
      <div className={style.header}>
        <div className={style.logohh}>
          <Image src="/images/selectlogo.svg" />
        </div>
        <div className={style.userdiv}>
          <p className={style.userinfo}>
            Welcome, <span className={style.userheading}>User!</span>
          </p>
        </div>
        <div className={style.crossbtn}>
          <Button variant="default">
            <Image src="/images/cross.svg" alt="" />
          </Button>
        </div>
      </div>
      <TabLink />
    </>
  );
}

export { Header };
