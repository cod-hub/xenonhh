import React from "react";
import style from "./common.module.scss";

function CommonLayout({ children }) {
  return <div className={style.commonmain}>{children}</div>;
}

export { CommonLayout };
