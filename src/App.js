import "bootstrap/dist/css/bootstrap.min.css";
import "./style/theme.css";
import "./App.css";
import { Header, CommonLayout } from "./components";
// import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

function App() {
  return (
    <>
      <div className="appmain">
        <CommonLayout>
          <Header />
        </CommonLayout>
      </div>
    </>
  );
}

export default App;
